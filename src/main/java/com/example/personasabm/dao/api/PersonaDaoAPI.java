package com.example.personasabm.dao.api;

import org.springframework.data.repository.CrudRepository;

public interface PersonaDaoAPI extends CrudRepository<com.example.personasabm.model.Persona, Long> {

}
