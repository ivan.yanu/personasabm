package com.example.personasabm.service.api;

import com.example.personasabm.commons.GenericServiceApi;
import com.example.personasabm.model.Persona;

public interface PersonaServiceAPI extends GenericServiceApi<Persona, Long>{
	 
}
