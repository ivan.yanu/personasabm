package com.example.personasabm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonasAbmApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersonasAbmApplication.class, args);
	}

}
